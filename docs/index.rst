.. MIDAS documentation master file, created by
   sphinx-quickstart on Thu Oct  8 08:55:26 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MIDAS's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   installation
   getting_started
   next_steps
   configuration
   modules/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
