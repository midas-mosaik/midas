.. MIDAS modules documentation

Overview of MIDAS' Default Modules
==================================

This section provides a detailed overview of the modules of MIDAS.
The order is based on the load order of modules, defined in the default *midas-runtime-conf.yml*.

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    store
    timesim
    powergrid
    sndata
    comdata

More modules will be described soon.